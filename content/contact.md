+++
date = "2016-06-12T19:38:48Z"
draft = false
title = "Contact"
sidemenu = "true"
+++

If you have any queries please contact me by <a href="mailto:t.ramirezdorner@gmail.com">email</a>.

You can also contact me on:

<ul class="fa-ul">
  <li>
    <a href="https://twitter.com/trpzn" target="_blank"><i class="fa fa-twitter-square fa-lg"></i>@trpzn</a>
  </li>
  <li>
    <a href="https://instagram.com/ttropezon" target="_blank"><i class="fa fa-instagram fa-lg"></i>@ttropezon</a>
  </li>
  <li>
    <a href="https://github.com/trpzn" target="_blank"><i class="fa fa-github-square fa-lg"></i>@trpzn</a>
  </li>
</ul>
