# tomas.nuve.cl

My personal website hosted on Gitlab Pages.

## Powered by
- [Hugo](//gohugo.io/)
- [Pure CSS](//purecss.io/)

## Built and Deployed with gitlab jobs

# Instructions

## Local

### Windows / Linux

install hugo-cli an run hugo server -D 
